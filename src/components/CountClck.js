 
import { Component } from "react"

class CountClick extends Component{
  constructor(prop) {
    super(prop);

    this.state = {
      count:0
    }

    // this.clickHandler = this.clickHandler.bind(this) 
  }
  // clickHandler(){
  //  this.setState({
  //   count: this.state.count + 1
  //  })
  // }

  clickHandler =() =>{
    this.setState({
    count: this.state.count + 1
   })
  }

 render(){
  return(
   <div>
     <p>Count: {this.state.count}</p>
     <button onClick={this.clickHandler}>Click here!</button>
   </div>

  )
 }
}

export default CountClick;